package com.epam.calculatorDemo;

import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		Calculator calculate = new Calculator();
		Scanner scan = new Scanner(System.in);
		System.out.println("enter number 1: ");
		int n1 = scan.nextInt();
		System.out.println("enter number 2: ");
		int n2 = scan.nextInt();
		int option;
		int res = 0;
		System.out.println("1. Addition");
		System.out.println("2. Subtraction");
	    System.out.println("3. Multiplication");
	    System.out.println("4. Division");
		do{
			System.out.println("enter the option: ");
			option = scan.nextInt();
			switch (option) {
			case 1:
				res = calculate.addNumbers(n1, n2);
				break;
			case 2:
				res = calculate.subtractNumbers(n1, n2);
				break;
			case 3:
				res = calculate.multiplyNumbers(n1, n2);
				break;
			case 4:
				res = calculate.divideNumbers(n1, n2);
				break;
			default:
				System.out.println("invalid");
				break;
			}
			System.out.println("result ="+res);
		}while(option != 4);

	}

}
